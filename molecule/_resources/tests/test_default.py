import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_service(host):
    srv = host.service("memcached")
    assert srv.is_running
    assert srv.is_enabled


def test_socket(host):
    host.socket("tcp://11211").is_listening
