# Ansible role for Memcached installation

## Introduction

[Memcached](http://www.memcached.org/) is a memory object caching
system, running as a daemon.

This role installs and configure the server.

You can specify the listening address (default to 127.0.0.1 to be
secure). The firewall configuration is left to your care.

## Usage

To read parameters documentation, use this command:

```
ansible-doc -t role memcached
```
